$(function () {
    new WOW({ offset: 100 }).init()
    // 輪播
    $.fn.randomize = function (selector) {
        var $elems = selector ? $(this).find(selector) : $(this).children(),
            $parents = $elems.parent()

        $parents.each(function () {
            $(this)
                .children(selector)
                .sort(function () {
                    return Math.round(Math.random()) - 0.5
                })
                .detach()
                .appendTo(this)
        })

        return this
    }
    $('.attractions').find('.carousel').randomize('.carousel-item')
    $('.carousel').slick({
        autoplay: true,
        autoplaySpeed: 4000,
        dots: false,
        arrows: true,
        infinite: true,
    })

    //捲動至錨點
    var $root = $('html, body')
    $('a[href^="#"]').click(function () {
        $root.animate(
            {
                scrollTop: $($.attr(this, 'href')).offset().top,
            },
            500,
        )
        return false
    })
})
